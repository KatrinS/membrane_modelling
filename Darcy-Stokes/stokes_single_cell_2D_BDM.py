from dolfin import *
from mshr import *

# more detailed output
dolfin.set_log_level(dolfin.TRACE)

# Load mesh
mesh = Mesh("single_cell_2D.xml")

##########

# Sub domain for no-slip (mark whole boundary, inflow and outflow will overwrite)
class Wall(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary

# Sub domain for inflow (left)
class Inflow(SubDomain):
    def inside(self, x, on_boundary):
        return x[0] < -7.5 + 50*DOLFIN_EPS and on_boundary

# Sub domain for outflow (left)
class Outflow(SubDomain):
    def inside(self, x, on_boundary):
        return x[0] > 90 - 50*DOLFIN_EPS and on_boundary

boundaries = FacetFunction('size_t', mesh)
boundaries.set_all(3)

# Mark no-slip facets as sub domain 0
wall = Wall()
wall.mark(boundaries, 0)

# Mark inflow as sub domain 1
inflow = Inflow()
inflow.mark(boundaries, 1)

# Mark outflow as sub domain 2
outflow = Outflow()
outflow.mark(boundaries, 2)

plot(boundaries)

###########


# Define function spaces
vector = FunctionSpace(mesh, "BDM", 1)
scalar = FunctionSpace(mesh, "DG", 0)
system = vector * scalar


# No-slip boundary condition for velocity
#bc = DirichletBC(system.sub(0), Constant((0, 0)), boundaries, 0)

# Measures with references to boundaries
ds = Measure('ds', domain=mesh, subdomain_data=boundaries) 

# Define variational problem
p1 = 3.77e-05
p2 = 0.0
mu = 1e-09
n = FacetNormal(mesh)

(v, q) = TestFunctions(system)
(u, p) = TrialFunctions(system)

h = CellSize(mesh)
#h1= 2*Circumradius(mesh) #equivalent to Cellsize

alpha = 10
wall = 0
inlet = 1
outlet = 2


a0 = (mu*inner(grad(u), grad(v)) - div(v)*p + q*div(u))*dx

innerfacets = - mu*inner(avg(grad(u))*n('+'), jump(v))*dS - mu*inner(avg(grad(v))*n('+'), jump(u))*dS \
				+ 2*avg(mu)*avg(alpha)/avg(h)*inner(jump(u), jump(v))*dS 

boundary = - mu*inner(grad(u)*n, v)*ds(wall) - mu*inner(grad(v)*n, u)*ds(wall) \
		    + 2*mu*2*alpha/h*inner(u, v)*ds(wall) \
			+ p*inner(v, n)*ds(wall) + q*inner(u, n)*ds(wall) 

a = a0 + innerfacets + boundary 
L = - p1*inner(v, n)*ds(inlet) - p2*inner(v, n)*ds(outlet)





# Compute solution
w = Function(system)
solve(a == L, w)#s, bc)
u, p = w.split()

velocity_file = File("velocity_single_cell.xml")
velocity_file << interpolate(u, vector)

# Save solution in VTK format
#ufile_pvd = File("velocity.pvd")
#ufile_pvd << u
#pfile_pvd = File("pressure.pvd")
#pfile_pvd << p

# Plot solution
plot(u, "u")
plot(p)
interactive()

