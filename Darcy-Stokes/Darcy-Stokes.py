from dolfin import * 

# Load mesh 
mesh = Mesh("./mesh.xml")

# Sub domain for no-slip wall
class Noslip(SubDomain):
	def inside(self, x, on_boundary):
		return on_boundary

# Sub domain for inflow (left)
class Inflow(SubDomain):
	def inside(self, x, on_boundary):
		return x[0] < DOLFIN_EPS and on_boundary

# Sub domain for outflow (left)
class Outflow(SubDomain):
	def inside(self, x, on_boundary):
		return x[0] > 0.02 - DOLFIN_EPS and on_boundary
			 
#Sub domain for circle (Darcy domain)
center = dolfin.Point(0.01, 0.01)
radius = 0.005

class Circle(SubDomain):
	def inside(self, x, on_boundary):
		r = sqrt((x[0] - center[0])**2 + (x[1] - center[1])**2)
		return r < radius + 0.0002
		  
# Sub domain for interface
class Interface(SubDomain):
	def inside(self, x, on_boundary):
		r = sqrt((x[0] - center[0])**2 + (x[1] - center[1])**2)
		return r > radius - 0.0002 and r < radius + 0.0002	  
		
# Create mesh functions over the cells and facets
subdomains_cells = CellFunction("size_t", mesh)
subdomains_facets = FacetFunction("size_t", mesh)

# Mark all cells as subdomain 0 (Stokes domain)
subdomains_cells.set_all(0)

# Mark circle as subdomain 1 (Darcy domain)
circle = Circle()
circle.mark(subdomains_cells, 1)

# Mark all facets as subdomain 0 (Stokes domain)
subdomains_facets.set_all(0)

# Mark facets in circle as subdomain 1 (Darcy domain)
circle = Circle()
circle.mark(subdomains_facets, 1)

# Mark no-slip facets as subdomain 2 (mark whole boundary, inflow and outflow will overwrite)
noslip = Noslip()
noslip.mark(subdomains_facets, 2)

# Mark inflow facets as subdomain 3
inflow = Inflow()
inflow.mark(subdomains_facets, 3)

# Mark outflow facets as subdomain 4
outflow = Outflow()
outflow.mark(subdomains_facets, 4)

# Mark interface facets as subdomain 5
interface = Interface()
interface.mark(subdomains_facets, 5)

plot(subdomains_cells)
plot(subdomains_facets)

#########################################################

# Define function spaces and mixed (product) space
BDM = FunctionSpace(mesh, "BDM", 1)
DG = FunctionSpace(mesh, "DG", 0)
W = BDM * DG
V = FunctionSpace(mesh, "DG", 0)

(v, q) = TestFunctions(W)
(u, p) = TrialFunctions(W)

# create concentration function 
c = Function(V)
File("concentration.xml") >> c
c = Constant(0)

# redefine measures
dx = Measure("dx", domain=mesh, subdomain_data=subdomains_cells)
ds = Measure("ds", domain=mesh, subdomain_data=subdomains_facets)
dS = Measure("dS", domain=mesh, subdomain_data=subdomains_facets)

# Domain labels
fluid_domain = 0
porous_domain = 1
wall = 2
inflow = 3
outflow = 4
interface = 5

# Set Dirichlet boundary condition for the normal component of the velocity at the wall
bc = DirichletBC(W.sub(0), Constant((0.0,0.0)), subdomains_facets, wall)

# Define variational problem
n = FacetNormal(mesh)
tau = as_vector((-n('+')[1], n('+')[0]))
h = CellSize(mesh)
#h1= 2*Circumradius(mesh) #equivalent to Cellsize

# Set constants
p_in = Constant(0.0151) #kg/(mm*s^2)
p_out = Constant(0.0)
mu = Constant(1e-06) #kg/(mm*s)
G = Constant(7e-3) #Constant(7e-7) #mm^2*s/kg
i = 1 
R = 8.3e6 #kg*mm^2/(s^2*mol*K)
T = 310 #K

#parameters that need to be found
alpha_w = Constant(10)
alpha_gamma = Constant(10)
beta = Constant(10)
K = Constant(1e-6) # m^2 #6.5e-18 # from Timos thesis TODO: replace value!!!


#('+') belongs to the cell in the subdomain with the bigger marking --> here, at the interface ('+') denotes a Darcy quantity, n('+') is the outward normal to the cell with the bigger marking
#jump(c) = c+ - c- = c_d - c_s

a0 = mu*inner(grad(u), grad(v))*dx(fluid_domain) - mu*inner(grad(u)*n, v)*ds(wall) - mu*inner(grad(v)*n, u)*ds(wall) \
	- mu*inner(avg(grad(u))*n('+'), jump(v))*dS(fluid_domain) - mu*inner(avg(grad(v))*n('+'), jump(u))*dS(fluid_domain) + mu*beta/avg(h)*inner(jump(u), jump(v))*dS(fluid_domain) \
	+ mu/K*inner(u, v)*dx(porous_domain)

b1 = - p*div(v)*dx(fluid_domain)- p*div(v)*dx(porous_domain)

b2 = - q*div(u)*dx(fluid_domain)- q*div(u)*dx(porous_domain)

c0 = - mu*inner(grad(u)('-')*tau,tau)*inner(n('-'),v('-'))*dS(interface) - 1/G*inner(u('-'),n('-'))* inner(n('-'),v('-'))*dS(interface) \
	- mu*inner(grad(v)('-')*tau,tau)*inner(n('-'),u('-'))*dS(interface) - 1/G*inner(v('-'),n('-'))* inner(n('-'),u('-'))*dS(interface) 


d0 = mu*alpha_w/h*inner(u, v)*ds(wall) + mu*alpha_gamma/avg(h)*inner(u('-'),tau)*inner(v('-'),tau)*dS(interface) 	 

zeros = inner(avg(p), jump(v,n))*dS(fluid_domain) + inner(avg(q), jump(u,n))*dS(fluid_domain) + p('+')*inner(v('+'),n('+'))*dS(interface) #TODO remove?

a = a0 + b1 + b2 + c0 + d0 #+ zeros
L = - p_in*inner(n, v)*ds(inflow) - p_out*inner(n, v)*ds(outflow) - i*R*T*(-jump(c))*inner(v('-'),n('-'))*dS(interface) + v[0]*dx(100) # dx(100) is undefined, so this term will not contribute to the integral, but dx being present means the assembler "knows" about subdomains_cells - if some term in a/L uses dx, you don't need this hack





# Compute solution
w = Function(W)
solve(a == L, w, bc)
u, p = w.split()

velocity_file = File("velocity.xml")
velocity_file << interpolate(u, BDM)

# Save solution in VTK format
ufile_pvd = File("results/velocity.pvd")
ufile_pvd << u
pfile_pvd = File("results/pressure.pvd")
pfile_pvd << p

# Plot solution
plot(u, "u")
plot(p)
interactive()





