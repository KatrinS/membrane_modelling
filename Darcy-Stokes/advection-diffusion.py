from dolfin import * 

# Load mesh 
mesh = Mesh("./mesh.xml")

# Sub domain for no-slip wall
class Noslip(SubDomain):
	def inside(self, x, on_boundary):
		return on_boundary

# Sub domain for inflow (left)
class Inflow(SubDomain):
	def inside(self, x, on_boundary):
		return x[0] < DOLFIN_EPS and on_boundary

# Sub domain for outflow (left)
class Outflow(SubDomain):
	def inside(self, x, on_boundary):
		return x[0] > 0.02 - DOLFIN_EPS and on_boundary
			 
#Sub domain for circle (Darcy domain)
center = dolfin.Point(0.01, 0.01)
radius = 0.005

class Circle(SubDomain):
	def inside(self, x, on_boundary):
		r = sqrt((x[0] - center[0])**2 + (x[1] - center[1])**2)
		return r < radius + 0.0002
		  
# Sub domain for interface
class Interface(SubDomain):
	def inside(self, x, on_boundary):
		r = sqrt((x[0] - center[0])**2 + (x[1] - center[1])**2)
		return r > radius - 0.0002 and r < radius + 0.0002	 
# Create mesh functions over the cells and facets
subdomains_cells = CellFunction("size_t", mesh)
subdomains_facets = FacetFunction("size_t", mesh)

# Mark all cells as subdomain 0 (Stokes domain)
subdomains_cells.set_all(0)

# Mark circle as subdomain 1 (Darcy domain)
circle = Circle()
circle.mark(subdomains_cells, 1)

# Mark all facets as subdomain 0 (Stokes domain)
subdomains_facets.set_all(0)

# Mark facets in circle as subdomain 1 (Darcy domain)
circle = Circle()
circle.mark(subdomains_facets, 1)

# Mark no-slip facets as subdomain 2 (mark whole boundary, inflow and outflow will overwrite)
noslip = Noslip()
noslip.mark(subdomains_facets, 2)

# Mark inflow facets as subdomain 3
inflow = Inflow()
inflow.mark(subdomains_facets, 3)

# Mark outflow facets as subdomain 4
outflow = Outflow()
outflow.mark(subdomains_facets, 4)

# Mark interface facets as subdomain 5
interface = Interface()
interface.mark(subdomains_facets, 5)

plot(subdomains_cells)
plot(subdomains_facets)

# Create FunctionSpaces
DG = FunctionSpace(mesh, "DG", 0)
BDM = FunctionSpace(mesh, "BDM", 1)

# redefine measures
dx = Measure("dx", domain=mesh, subdomain_data=subdomains_cells)
ds = Measure("ds", domain=mesh, subdomain_data=subdomains_facets)
dS = Measure("dS", domain=mesh, subdomain_data=subdomains_facets)

################################################

# Create velocity Function from file
velocity = Function(BDM);
File("velocity.xml") >> velocity
#velocity = Constant((0.1, 0))

# Initialise source function and previous solution function
f  = Constant(0.0)
c0 = Function(DG)

# Test and trial functions
c, phi = TrialFunction(DG), TestFunction(DG)

# Mid-point solution
c_mid = 0.5*(c0 + c)

h = CellSize(mesh)
n = FacetNormal(mesh)

# Residual
#r = c - c0 + delta_t*(inner(velocity, grad(c_mid)) - D*div(grad(c_mid)) - f)

# Parameters
T = 0.01
delta_t = 0.01
D = 5e-4 #(mm)^2/s

#concentration difference the cell tries to maintain
c_diff = 6e-8 #mol/(mm)^3 #TODO too small numbers, change everything?

#concentration at inflow boundary
c_in = 0 #10e-8  #TODO change to realistic value

beta_ad = Constant(10)
alpha_ad = Constant(1)


# Domain labels
fluid_domain = 0
porous_domain = 1
wall = 2
inflow = 3
outflow = 4
interface = 5

# Variational problem
F = phi*(c - c0)*dx + delta_t*(phi*inner(velocity, grad(c_mid))*dx \
                      + D*inner(grad(phi), grad(c_mid))*dx)

# IP
F += - delta_t*D*inner(avg(grad(c_mid))*jump(phi),n('+'))*dS(fluid_domain) \
		- delta_t*D*inner(avg(grad(c_mid))*jump(phi),n('+'))*dS(porous_domain) \
		- delta_t*D*inner(avg(grad(c_mid))*jump(phi),n('+'))*dS(interface) 
	
# IP symmetry
F += - delta_t*D*inner(avg(grad(phi))*jump(c_mid),n('+'))*dS(fluid_domain) \
		- delta_t*D*inner(avg(grad(phi))*jump(c_mid),n('+'))*dS(porous_domain) \
		- delta_t*D*inner(avg(grad(phi))*jump(c_mid),n('+'))*dS(interface)

# IP penalty
F += - delta_t*D*beta_ad/avg(h) * jump(c_mid)*jump(phi)*dS(fluid_domain) \
		- delta_t*D*beta_ad/avg(h) * jump(c_mid)*jump(phi)*dS(porous_domain) \
		- delta_t*D*beta_ad/avg(h) * jump(c_mid)*jump(phi)*dS(interface)

# maintained concentration difference
F += delta_t*D*inner(avg(grad(phi))*c_diff,n('+'))*dS(interface) \
		- delta_t*D*beta_ad/avg(h) *c_diff*jump(phi)*dS(interface)
		
# Dirichlet condition (Nitsche)
F += delta_t*alpha_ad/h*(c_mid - c_in)*phi*ds(inflow)
	

# Add SUPG stabilisation terms
#vnorm = sqrt(inner(velocity, velocity))
#h = CellSize(mesh)
#F += (h/(2.0*vnorm))*inner(velocity, grad(phi))*r*dx

# Create bilinear and linear forms
a = lhs(F)
L = rhs(F)

# Assemble matrix
A = assemble(a)

# Create linear solver and factorize matrix
solver = LUSolver(A)
solver.parameters["reuse_factorization"] = True

# Output file
#out_file = File("results/concentration.pvd")
concentration_file = File("concentration.xml")

# Set intial condition
c = c0

# Time-stepping
t = 0.0
while t <= T:

    # Assemble vector and apply boundary conditions
    b = assemble(L)

    # Solve the linear system (re-use the already factorized matrix A)
    solver.solve(c.vector(), b)
    #solve(a == L, c, bc)

    # Copy solution from previous interval
    c0 = c

    # Plot solution
    time = str(t)
    plot(c, key='c', title = time)

    # Save the solution to file
#    out_file << (c, t)
    concentration_file << interpolate(c, DG)

    # Move to next interval and adjust boundary condition
    t += delta_t

# Hold plot
interactive()
