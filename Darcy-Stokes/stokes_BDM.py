from dolfin import *

# more detailed output
dolfin.set_log_level(dolfin.TRACE)

# Load mesh 
mesh = Mesh("./mesh.xml")

# Sub domain for no-slip
class Noslip(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary

# Sub domain for inflow (left)
class Inflow(SubDomain):
    def inside(self, x, on_boundary):
        return x[0] < DOLFIN_EPS and on_boundary

# Sub domain for outflow (left)
class Outflow(SubDomain):
    def inside(self, x, on_boundary):
        return x[0] > 1-DOLFIN_EPS and on_boundary
             
#Sub domain for circle (Darcy domain)
center = dolfin.Point(0.5, 0.5)
radius = 0.25
class Circle(SubDomain):
    def inside(self, x, on_boundary):
	r = sqrt((x[0] - center[0])**2 + (x[1] - center[1])**2)
        return r < radius + 0.02
          
# Sub domain for interface
class Interface(SubDomain):
    def inside(self, x, on_boundary):
	r = sqrt((x[0] - center[0])**2 + (x[1] - center[1])**2)
        return r > radius - 0.02 and r < radius + 0.02      
        
# Create mesh functions over the cells and facets
subdomains_cells = CellFunction("size_t", mesh)
subdomains_facets = FacetFunction("size_t", mesh)

# Mark all cells as subdomain 0 (Stokes domain)
subdomains_cells.set_all(0)

# Mark circle as subdomain 1 (Darcy domain)
circle = Circle()
circle.mark(subdomains_cells, 1)

# Mark no-slip facets as subdomain 2 (mark whole boundary, inflow and outflow will overwrite)
noslip = Noslip()
noslip.mark(subdomains_facets, 2)

# Mark inflow facets as subdomain 3
inflow = Inflow()
inflow.mark(subdomains_facets, 3)

# Mark outflow facets as subdomain 4
outflow = Outflow()
outflow.mark(subdomains_facets, 4)

# Mark interface facets as subdomain 5
interface = Interface()
interface.mark(subdomains_facets, 5)

plot(subdomains_cells)
plot(subdomains_facets)

###########


# Define function spaces
vector = FunctionSpace(mesh, "BDM", 1)
scalar = FunctionSpace(mesh, "DG", 0)
system = vector * scalar


# No-slip boundary condition for velocity
bc = DirichletBC(system.sub(0), Constant((0, 0)), subdomains_facets, 2)

# redefine measures
dx = Measure("dx", domain=mesh, subdomain_data=subdomains_cells)
ds = Measure("ds", domain=mesh, subdomain_data=subdomains_facets)
dS = Measure("dS", domain=mesh, subdomain_data=subdomains_facets)

# Define variational problem
p1 = 3.77e-05
p2 = 0.0
mu = 1e-09
n = FacetNormal(mesh)

(v, q) = TestFunctions(system)
(u, p) = TrialFunctions(system)

h = CellSize(mesh)
#h1= 2*Circumradius(mesh) #equivalent to Cellsize

alpha = 10

fluid_domain = 0
wall = 2
inlet = 3
outlet = 4


a0 = (mu*inner(grad(u), grad(v)) - div(v)*p + q*div(u))*dx(fluid_domain)

innerfacets = - mu*inner(avg(grad(u))*n('+'), jump(v))*dS(fluid_domain) - mu*inner(avg(grad(v))*n('+'), jump(u))*dS(fluid_domain) \
				+ 2*avg(mu)*avg(alpha)/avg(h)*inner(jump(u), jump(v))*dS(fluid_domain) 

boundary = - mu*inner(grad(u)*n, v)*ds(wall) - mu*inner(grad(v)*n, u)*ds(wall) \
		    + 2*mu*2*alpha/h*inner(u, v)*ds(wall) \
			+ p*inner(v, n)*ds(wall) + q*inner(u, n)*ds(wall) 

a = a0 + innerfacets + boundary 
L = - p1*inner(v, n)*ds(inlet) - p2*inner(v, n)*ds(outlet)





# Compute solution
w = Function(system)
solve(a == L, w)#s, bc)
u, p = w.split()

velocity_file = File("velocity.xml")
velocity_file << interpolate(u, vector)

# Save solution in VTK format
#ufile_pvd = File("velocity.pvd")
#ufile_pvd << u
#pfile_pvd = File("pressure.pvd")
#pfile_pvd << p

# Plot solution
plot(u, "u")
plot(p)
interactive()

