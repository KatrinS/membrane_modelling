from dolfin import * 

# Load mesh 
mesh = Mesh("./mesh.xml")

# Sub domain for no-slip
class Noslip(SubDomain):
	def inside(self, x, on_boundary):
		return on_boundary

# Sub domain for inflow (left)
class Inflow(SubDomain):
	def inside(self, x, on_boundary):
		return x[0] < DOLFIN_EPS and on_boundary

# Sub domain for outflow (left)
class Outflow(SubDomain):
	def inside(self, x, on_boundary):
		return x[0] > 1-DOLFIN_EPS and on_boundary
			 
#Sub domain for circle (Darcy domain)
center = dolfin.Point(0.5, 0.5)
radius = 0.25
class Circle(SubDomain):
	def inside(self, x, on_boundary):
		r = sqrt((x[0] - center[0])**2 + (x[1] - center[1])**2)
		return r < radius + 0.02
		  
# Sub domain for interface
class Interface(SubDomain):
	def inside(self, x, on_boundary):
		r = sqrt((x[0] - center[0])**2 + (x[1] - center[1])**2)
		return r > radius - 0.02 and r < radius + 0.02	  
		
# Create mesh functions over the cells and facets
subdomains_cells = CellFunction("size_t", mesh)
subdomains_facets = FacetFunction("size_t", mesh)

# Mark all cells as subdomain 0 (Stokes domain)
subdomains_cells.set_all(0)

# Mark circle as subdomain 1 (Darcy domain)
circle = Circle()
circle.mark(subdomains_cells, 1)

# Mark all facets as subdomain 0 (Stokes domain)
subdomains_facets.set_all(0)

# Mark facets in circle as subdomain 1 (Darcy domain)
circle = Circle()
circle.mark(subdomains_facets, 1)

# Mark no-slip facets as subdomain 2 (mark whole boundary, inflow and outflow will overwrite)
noslip = Noslip()
noslip.mark(subdomains_facets, 2)

# Mark inflow facets as subdomain 3
inflow = Inflow()
inflow.mark(subdomains_facets, 3)

# Mark outflow facets as subdomain 4
outflow = Outflow()
outflow.mark(subdomains_facets, 4)

# Mark interface facets as subdomain 5
interface = Interface()
interface.mark(subdomains_facets, 5)

plot(subdomains_cells)
plot(subdomains_facets)

#########################################################
V = VectorFunctionSpace(mesh, "CG", 1)

u = TrialFunction(V)
v = TestFunction(V)
n = FacetNormal(mesh)

dx = Measure("dx", domain=mesh, subdomain_data=subdomains_cells)
dS = Measure("dS", domain=mesh, subdomain_data=subdomains_facets)
a = inner(u,v)*dx 
L = inner(n('-'),avg(v))*dS(5) + v[0]*dx(100) # dx(100) is undefined, so this term will not contribute to the integral, but dx being present means the assembler "knows" about subdomains_cells - if some term in a/L uses dx, you don't need this hack

b = assemble(L)
print b.array() 

U = Function(V) 

solve(a==L, U)

print U.vector().array()

plot(U)
interactive()





