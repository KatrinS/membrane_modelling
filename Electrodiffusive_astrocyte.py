from dolfin import *

#set_log_level(1)

l = 3.00e-4 		# Length of cell (m)

### Mesh, inflow_domain, function spaces and bounda conditions
mesh = IntervalMesh(100, 0, l)

# Sub domain for inflow (x<l/10)
class Inflow(SubDomain):
    def inside(self, x, on_boundary):
        return x[0] <= (l/10)      
        
        

# Mark subdomains
domains = MeshFunction('size_t', mesh, 0)
domains.set_all(0)

inflow = Inflow()
inflow.mark(domains, 1)

# Define measure
dx = Measure('dx', domain=mesh, subdomain_data=domains)

#plot(domains)


# Define function spaces
V = FunctionSpace(mesh, "CG", 1)
system = MixedFunctionSpace([V,V,V,V,V,V])

# Define functions and test functions
w = Function(system)
w_old = Function(system)

c_Ki, c_Ke, c_Nai, c_Nae, c_Cli, c_Cle = split(w)
c_Ki_old, c_Ke_old, c_Nai_old, c_Nae_old, c_Cli_old, c_Cle_old = split(w_old)

phi1, phi2, phi3, phi4, phi5, phi6 = TestFunctions(system) 


### Parameters
# PHYSICAL CONSTANTS (Units: mol, m, V, s, C, ohm, A)
R = 8.315 			# Gas constant  J*mol**-1*K**-1
F = 96500 			# Faraday's constant (C/mol)
T = 298 			# Temperature, K
psi = R*T/F 		# Standard potential (V)

# GEOMETRICAL PARAMETERS
# Notation here: e means outside (ECS), i means inside (intracellular)
A_i = 0.4 			# Chen&Nicholson (Tissue volume fraction being astrocytes)
A_e = 0.2 			# Chen&Nicholson (Tissue volume fraction being ECS)
O_M = A_i/5.00e-8 	# (Astrocytic membrane area per tissue volume)

# MEMBRANE PARAMETERS
g_Na = 1 			# Na+ conductance (S*m**-2)
g_Cl = 0.5 			# Cl- concuctance (S*m**-2)
g_K = 16.96 		# K+ conductance (S*m**-2)
C_m = 1.00e-2		# Membrane capacitance (Farad/m**2)

# ELECTRODIFFUSION PARAMETERS (from Grodzinski-book)
D_K = 1.96e-9 		# Diffusion coefficients (m**2/s)
D_Cl = 2.03e-9
D_Na = 1.33e-9
lambda_e = 1.6 		# ECS tortuousity C&N 
lambda_i = 3.2 		# Intracellular tortuousity C&N

### Initial conditions (mol/m**3 = mM)
# c_kn0 = Value from literature + correction (giving steady state)
c_Ke0 = 3.0 + 0.082
c_Ki0 = 100.0 - 0.041
c_Nae0 = 145.0 - 0.338
c_Nai0 = 15.0 + 0.189
c_Cle0 = 134.0 - 0.29
c_Cli0 = 5.0 + 0.145
v_m0 = -83.6e-3  	# Membrane pot. (V)


## Apply initial conditions 
print "Applying intitial conditions:"
c_Ki00 = project(c_Ki0, V)
c_Ke00 = project(c_Ke0, V)
c_Nai00 = project(c_Nai0, V)
c_Nae00 = project(c_Nae0, V)
c_Cli00 = project(c_Cli0, V)
c_Cle00 = project(c_Cle0, V)

assign(w_old, [c_Ki00, c_Ke00, c_Nai00, c_Nae00, c_Cli00, c_Cle00])
assign(w, [c_Ki00, c_Ke00, c_Nai00, c_Nae00, c_Cli00, c_Cle00])	
	
### Calculating the terms

# Calculating v_m from intracellular charge
rho_sI = (O_M/A_i)*C_m*v_m0 - F*(c_Ki0 + c_Nai0 - c_Cli0)
v_m = A_i/(O_M*C_m)*(F*(c_Ki + c_Nai - c_Cli) + rho_sI)
vmgrad = A_i/(O_M*C_m)*(F*(c_Ki.dx(0) + c_Nai.dx(0) - c_Cli.dx(0))) # v_m gradients

# Calculating e_k
e_K = psi*ln(c_Ke/c_Ki) 			# K+ reversal potential (V)
e_Na = psi*ln(c_Nae/c_Nai) 			# K+ reversal potential (V)
e_Cl = psi*ln(c_Cle/c_Cli) 			# K+ reversal potential (V)
 

# Calculating the Kir factor (unitless)	
e_Ke0 = psi*ln(c_Ke0/c_Ki0) 		# K+ reversal potential during rest (V)
delta_v = 1000*(v_m - e_K)
v_m_mil = 1000*v_m					# converting to mV
e_Ke0_mil = 1000*e_Ke0
fakt1 = (1 + exp(18.5/42.4)) / (1 + exp((delta_v + 18.5)/42.4))
fakt2 = (1 + exp(-(118.6 + e_Ke0_mil)/44.1)) / (1 + exp(-(118.6 + v_m_mil)/44.1))

f_Kir = sqrt(c_Ke/c_Ke0)*fakt1*fakt2


# The pump rate for the Na/K pump

K_mNai = 10.0 		# K+ threshold concentration for Na/K-pump (mol/m**3) 
K_mKe = 1.5  		# Na+ threshold concentration for Na/K-pump (mol/m**3) 
Pmax = 1.115e-6  	# Maximum pumprate, mol/(m**2*s) 
P = Pmax*(c_Ke/(c_Ke + K_mKe))*c_Nai**1.5/(c_Nai**1.5 + K_mNai**1.5)  #(mol/(m**2*s))

# Membrane flux densities (mol/(m**2*s))
j_KM = (g_K*f_Kir/F) * (v_m - e_K) - 2*P
j_NaM = (g_Na/F) * (v_m - e_Na) + 3*P
j_ClM = -(g_Cl/F) * (v_m + e_Cl)

# Flux densities due to diffusion (mol/(m**2*s))
j_KeD = -(D_K/lambda_e**2)*c_Ke.dx(0)
j_KiD = -(D_K/lambda_i**2)*c_Ki.dx(0)
j_NaeD = -(D_Na/lambda_e**2)*c_Nae.dx(0)
j_NaiD = -(D_Na/lambda_i**2)*c_Nai.dx(0)
j_CleD = -(D_Cl/lambda_e**2)*c_Cle.dx(0)
j_CliD = -(D_Cl/lambda_i**2)*c_Cli.dx(0)

# Current densities due to diffusion (A/m**2)
i_ediff = F*(j_KeD + j_NaeD - j_CleD)
i_idiff = F*(j_KiD + j_NaiD - j_CliD)

# Resistivities (Ohm*m)
r_e = psi*lambda_e**2/(F*(D_Na*c_Nae + D_K*c_Ke + D_Cl*c_Cle))
r_i = psi*lambda_i**2/(F*(D_Na*c_Nai + D_K*c_Ki + D_Cl*c_Cli))

# Gradients of v_e (ECS) and v_i (ICS), units (V/m)
dv_edx = (-vmgrad + r_i*i_idiff + r_i*(A_e/A_i)*i_ediff)*r_e/(r_e + r_i*A_e/A_i)
dv_idx = (vmgrad + r_e*(A_i/A_e)*i_idiff + r_e*i_ediff)*r_i/(r_i + r_e*A_i/A_e)

# Flux densities due to elecrical migration(mol/(m**2*s))
j_KeV = -(D_K/lambda_e**2) * (1/psi)*c_Ke*dv_edx
j_KiV = -(D_K/lambda_i**2) * (1/psi)*c_Ki*dv_idx
j_NaeV = -(D_Na/lambda_e**2) * (1/psi)*c_Nae*dv_edx
j_NaiV = -(D_Na/lambda_i**2) * (1/psi)*c_Nai*dv_idx
j_CleV = (D_Cl/lambda_e**2) * (1/psi)*c_Cle*dv_edx
j_CliV = (D_Cl/lambda_i**2) * (1/psi)*c_Cli*dv_idx

# Total flux densities
j_Ki = j_KiD + j_KiV
j_Ke = j_KeD + j_KeV
j_Nai = j_NaiD + j_NaiV
j_Nae = j_NaeD + j_NaeV
j_Cli = j_CliD + j_CliV
j_Cle = j_CleD + j_CleV



### Solving and time stepping

dt = 0.003
t = dt
simt = 0.01

# INPUT and OUTPUT:

tstart = 10 			# Input start and end (s)
tstop = 45

Imax = 5.5e-7 			# Input amplitude K/Na-(exchange) (mol/(s m^2))
Kdec = 2.9e-8 			# Output factor (m/s)

#TODO evaluate every time step	
	
if t > tstart and t < tstop: 
	j_Ke_in =  - Imax  				# In the input zone, in the input time-window
else:
	j_Ke_in = 0.0	

	
j_Ke_out =  Kdec*(c_Ke - c_Ke0) 	# Decay towards resting concentrations

j_Nae_in = -j_Ke_in
j_Nae_out = -j_Ke_out



# variational formulations
F_K = dt* (O_M/A_i * j_KM * phi1 * dx  - j_Ki * phi1.dx(0) * dx) + (c_Ki - c_Ki_old) * phi1 * dx \
		+ dt * (-O_M/A_e * j_KM * phi2 * dx + O_M/A_e * j_Ke_in * phi2 * dx(1) + O_M/A_e * j_Ke_out * phi2 * dx - j_Ke * phi2.dx(0) * dx) + (c_Ke - c_Ke_old) * phi2 * dx 
	
F_Na = dt * (O_M/A_i * j_NaM * phi3 * dx - j_Nai * phi3.dx(0) * dx) + (c_Nai - c_Nai_old) * phi3 * dx \
		+ dt * (-O_M/A_e * j_NaM * phi4 * dx + O_M/A_e * j_Nae_in * phi4 * dx(1) + O_M/A_e * j_Nae_out * phi4 * dx - j_Nae * phi4.dx(0) * dx) + (c_Nae - c_Nae_old) * phi4 * dx 
		
F_Cl = dt * (O_M/A_i * j_ClM * phi5 * dx - j_Cli * phi5.dx(0) * dx) + (c_Cli - c_Cli_old) * phi5 * dx \
		+ dt * (-O_M/A_e * j_ClM * phi6 * dx - j_Cle * phi6.dx(0) * dx) + (c_Cle - c_Cle_old) * phi6 * dx 

FF = F_K + F_Na + F_Cl	


#Modifications for using specified solver
bc=[]
# Compute Jacobian form
J = derivative(FF, w)

problem = NonlinearVariationalProblem(FF, w, bc, J)
solver = NonlinearVariationalSolver(problem)
parameters["krylov_solver"]["monitor_convergence"] = True
parameters["krylov_solver"]["report"] = True		
parameters["krylov_solver"]["nonzero_initial_guess"] = True	

solver.parameters["newton_solver"]["relaxation_parameter"] = 1.0
solver.parameters["newton_solver"]["relative_tolerance"] = 1.0e-4	
solver.parameters["newton_solver"]["absolute_tolerance"] = 1.0e-8
solver.parameters["newton_solver"]["maximum_iterations"] = 100



plot(c_Ke-c_Ke00)
plot(j_KM)




#import IPython
#IPython.embed()


while t < simt + DOLFIN_EPS:
	solver.solve()
	t += dt
	w_old.assign(w)
	

	time = str(t)
	plot(c_Ke-c_Ke00, key='c_Ke', title = time)	
	plot(j_KM, key='j_KM', title = time)




interactive()

