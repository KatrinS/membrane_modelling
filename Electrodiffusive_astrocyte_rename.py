from dolfin import *

#set_log_level(1)

l = 3.00e-4 #Length of cell (m)
### Mesh, inflow_domain, function spaces and bounda conditions
mesh = IntervalMesh(400, 0, l)

# Sub domain for inflow (x<l/10)
class Inflow(SubDomain):
    def inside(self, x, on_boundary):
        return x[0] <= (l/10)      

# Mark subdomains
domains = MeshFunction('size_t', mesh, 0)
domains.set_all(0)

inflow = Inflow()
inflow.mark(domains, 1)

# Define measure
dx = Measure('dx', domain=mesh, subdomain_data=domains)

#plot(domains)


# Define function spaces
V = FunctionSpace(mesh, "CG", 1)
system = MixedFunctionSpace([V,V,V,V,V,V])

# Define functions and test functions
w = Function(system)
w_old = Function(system)

c_Ki, c_Ko, c_Nai, c_Nao, c_Cli, c_Clo = split(w)
c_Ki_old, c_Ko_old, c_Nai_old, c_Nao_old, c_Cli_old, c_Clo_old = split(w_old)

phi1, phi2, phi3, phi4, phi5, phi6 = TestFunctions(system) 

dt = 0.003
t = dt
simt = 10


### Parameters
# PHYSICAL CONSTANTS (Units: mol, m, V, s, C, ohm, A)
R = 8.315 			# Gas constant  J*mol**-1*K**-1
F = 96500 			# Faraday's constant (C/mol)
T = 298 			# Temperature, K
psifac = R*T/F 		# Standard potential (V)

#### GEOMETRICAL PARAMETERS
# Notation here: o means outside (ECS), i means inside (intracellular)
l = 3.00e-4 #Length of cell (m)
A_i = 0.4 # Chen&Nicholson (Tissue volume fraction being astrocytes)
A_o = 0.2 #Chen&Nicholson (Tissue volume fraction being ECS)
O_m = A_i/5.00e-8 # (Astrocytic membrane area per tissue volume)

#### INPUT PARAMETERS
tstart = 100 
tstop = 400 # Input starts and ends (s)
#tstart = 10 tstop = 45 # Used with compare_models.m
xstart = 0 
xstop = l*0.1 # Region for input (m)

Imax = 5.5e-7 # Input amplitude K/Na-(exchange) (mol/(s m**2))
Kdec = 2.9e-8 # Output factor (m/s)

#### DISCRETISATION
#simt = 600 # Simulate for 600 s
#xres = 1*100 x = linspace(0,l,xres) # 100 x-points
#tres = 1*100  t = linspace(0,simt,tres) # 100 t-points


# MEMBRANE PARAMETERS
g_Na = 1 			# Na+ conductance (S*m**-2)
g_Cl = 0.5 			# Cl- concuctance (S*m**-2)
g_K0 = 16.96 		# K+ conductance (S*m**-2)
C_m = 1.00e-2		# Membrane capacitance (Farad/m**2)

# ELECTRODIFFUSION PARAMETERS (from Grodzinski-book)
D_K = 1.96e-9 		# Diffusion coefficients (m**2/s)
D_Cl = 2.03e-9
D_Na = 1.33e-9
lambda_o = 1.6 		# ECS tortuousity C&N 
lambda_i = 3.2 		# Intracellular tortuousity C&N

### Initial conditions (mol/m**3 = mM)
# c_kn0 = Value from literature + correction (giving steady state)
c_Ko0 = 3.0 + 0.082
c_Ki0 = 100.0 - 0.041
c_Nao0 = 145.0 - 0.338
c_Nai0 = 15.0 + 0.189
c_Clo0 = 134.0 - 0.29
c_Cli0 = 5.0 + 0.145
v_m0 = -83.6e-3  	# Membrane pot. (V)


## Apply initial conditions 
print "Applying intitial conditions:"
c_Ki00 = project(c_Ki0, V)
c_Ko00 = project(c_Ko0, V)
c_Nai00 = project(c_Nai0, V)
c_Nao00 = project(c_Nao0, V)
c_Cli00 = project(c_Cli0, V)
c_Clo00 = project(c_Clo0, V)

assign(w_old, [c_Ki00, c_Ko00, c_Nai00, c_Nao00, c_Cli00, c_Clo00])
assign(w, [c_Ki00, c_Ko00, c_Nai00, c_Nao00, c_Cli00, c_Clo00])	
	
### Calculating the terms

X_iZ_i = -(F*A_i/O_m)*(c_Ki0 + c_Nai0 - c_Cli0) + C_m*v_m0 #Intracellular static charge (C/m**2)
X_oZ_o = -(F*A_o/O_m)*(c_Ko0 + c_Nao0 - c_Clo0) - C_m*v_m0 #ECS static charge (C/m**2)

### Algebraic method (define V_m in terms of ion concentrations)
v_mA = -(F*(A_o/O_m)/C_m*(c_Ko+c_Nao-c_Clo)+ X_oZ_o/C_m) #v_m derived from extracellular charge
v_mB = +(F*(A_i/O_m)/C_m*(c_Ki+c_Nai-c_Cli)+ X_iZ_i/C_m) #v_m defived from intracellular charge
v_m = v_mA # Random choice (as v_mA = v_mB)
vmgrad_A = -F*(A_o/O_m)/C_m*(c_Ko.dx(0) + c_Nao.dx(0) - c_Clo.dx(0)) # v_m gradient
vmgrad_B = +F*(A_i/O_m)/C_m*(c_Ki.dx(0) + c_Nai.dx(0) - c_Cli.dx(0)) # v_m gradients
vmgrad = vmgrad_A # Random choice (as v_mA = v_mB)

### Differential method (compute v_M from differential equation)
#    v_mA = u(7)
#    v_mB = u(8)
#    vmgrad_A = DuDx(7)
#    vmgrad_B = DuDx(8)
#    v_m = v_mA # Random choice (Should have: v_mi = v_mo)
#    vmgrad = vmgrad_A

### MEMBRANE MECHANISMS
# Kir-faktor for K+ conductance (unitless)
# Returns the Kir-factor for the K+ conductance

e_K=psifac*ln(c_Ko/c_Ki) # K+ reversal potential (V)
e_Ko0 = psifac*ln(c_Ko0/c_Ki0) # K+ reversal potential during rest (V)

# converting to mV
delta_v = 1000*(v_m - e_K)
v_m_mil = 1000*v_m
e_Ko0_mil = 1000*e_Ko0

fakt1=(1+exp(18.5/42.4))/(1+exp((delta_v + 18.5)/42.4))
fakt2=(1+exp(-(118.6+e_Ko0_mil)/44.1))/(1+exp(-(118.6+v_m_mil)/44.1))

f_Kir = sqrt(c_Ko/c_Ko0)*fakt1*fakt2

# Na/K-pumperate
# The pump rate for the Na/K pump

K_mNai=10.0# K+ threshold concentration for Na/K-pump (mol/m**3)
K_mKo=1.5 #Na+ threshold concentration for Na/K-pump (mol/m**3)
Pmax = 1.115e-6 #Maximum pumprate, mol/(m**2*s)
P = Pmax*(c_Ko/(c_Ko+K_mKo))*c_Nai**1.5/(c_Nai**1.5+K_mNai**1.5) #(mol/(m**2*s))

### INPUT:
#if t>tstart and t<tstop:
#    ft = Imax - Kdec*(c_Ko-c_Ko0) # In the input zone, in the input time-window
#else:
#    ft = - Kdec*(c_Ko-c_Ko0) # Decay towards resting concentrations
#end

# Membrane flux densities (mol/(m**2*s))
j_KM = (g_K0*f_Kir/F)*(v_m-psifac*ln(c_Ko/c_Ki)) - 2*P
j_NaM = (g_Na/F)*(v_m-psifac*ln(c_Nao/c_Nai)) + 3*P
j_ClM = -(g_Cl/F)*(v_m+psifac*ln(c_Clo/c_Cli))

j_KM = 0

# Membrane current density (A/m**2)
i_m = F*(j_KM+j_NaM-j_ClM)

# Flux densities due to diffusion (mol/(m**2*s))
j_KoD = -(D_K/lambda_o**2)*c_Ko.dx(0)
j_KiD = -(D_K/lambda_i**2)*c_Ki.dx(0)
j_NaoD = -(D_Na/lambda_o**2)*c_Nao.dx(0)
j_NaiD = -(D_Na/lambda_i**2)*c_Nai.dx(0)
j_CloD = -(D_Cl/lambda_o**2)*c_Clo.dx(0)
j_CliD = -(D_Cl/lambda_i**2)*c_Cli.dx(0)

# Current densities due to diffusion (A/m**2)
i_odiff = F*(j_KoD + j_NaoD - j_CloD)
i_idiff = F*(j_KiD + j_NaiD - j_CliD)

# Resistivities (Ohm*m)
r_o = psifac*lambda_o**2/(F*(D_Na*c_Nao+D_K*c_Ko+D_Cl*c_Clo))
r_i = psifac*lambda_i**2/(F*(D_Na*c_Nai+D_K*c_Ki+D_Cl*c_Cli))

#Gradients of v_o (ECS) and v_i (ICS), units (V/m)
dv_odx = (-vmgrad+r_i*i_idiff+r_i*(A_o/A_i)*i_odiff)*r_o/(r_o+r_i*A_o/A_i)
dv_idx = (vmgrad+r_o*(A_i/A_o)*i_idiff+r_o*i_odiff)*r_i/(r_i+r_o*A_i/A_o)

# Flux densities due to elecrical migration(mol/(m**2*s))
j_KoV = -(D_K/lambda_o**2)*(1/psifac)*c_Ko*dv_odx
j_KiV = -(D_K/lambda_i**2)*(1/psifac)*c_Ki*dv_idx
j_NaoV = -(D_Na/lambda_o**2)*(1/psifac)*c_Nao*dv_odx
j_NaiV = -(D_Na/lambda_i**2)*(1/psifac)*c_Nai*dv_idx
j_CloV = (D_Cl/lambda_o**2)*(1/psifac)*c_Clo*dv_odx
j_CliV = (D_Cl/lambda_i**2)*(1/psifac)*c_Cli*dv_idx

# Current densities due to electrical migration (A/m**2)
i_ofield = F*(j_KoV + j_NaoV - j_CloV)
i_ifield = F*(j_KiV + j_NaiV - j_CliV)

# Total flux densities
j_Ki = j_KiD + j_KiV
j_Ko = j_KoD + j_KoV
j_Nai = j_NaiD + j_NaiV
j_Nao = j_NaoD + j_NaoV
j_Cli = j_CliD + j_CliV
j_Clo = j_CloD + j_CloV



# INPUT and OUTPUT:

#TODO evaluate every time step	
if t > tstart and t < tstop: 
	j_Ko_in =  - Imax  				# In the input zone, in the input time-window
else:
	j_Ko_in = 0.0	

	
j_Ko_out =  Kdec*(c_Ko - c_Ko0) 	# Decay towards resting concentrations

j_Nao_in = -j_Ko_in
j_Nao_out = -j_Ko_out



# variational formulations
F_K = dt* (O_m/A_i * j_KM * phi1 * dx  - j_Ki * phi1.dx(0) * dx) + (c_Ki - c_Ki_old) * phi1 * dx \
		+ dt * (-O_m/A_o * j_KM * phi2 * dx + O_m/A_o * j_Ko_in * phi2 * dx(1) + O_m/A_o * j_Ko_out * phi2 * dx - j_Ko * phi2.dx(0) * dx) + (c_Ko - c_Ko_old) * phi2 * dx 
	
F_Na = dt * (O_m/A_i * j_NaM * phi3 * dx - j_Nai * phi3.dx(0) * dx) + (c_Nai - c_Nai_old) * phi3 * dx \
		+ dt * (-O_m/A_o * j_NaM * phi4 * dx + O_m/A_o * j_Nao_in * phi4 * dx(1) + O_m/A_o * j_Nao_out * phi4 * dx - j_Nao * phi4.dx(0) * dx) + (c_Nao - c_Nao_old) * phi4 * dx 
		
F_Cl = dt * (O_m/A_i * j_ClM * phi5 * dx - j_Cli * phi5.dx(0) * dx) + (c_Cli - c_Cli_old) * phi5 * dx \
		+ dt * (-O_m/A_o * j_ClM * phi6 * dx - j_Clo * phi6.dx(0) * dx) + (c_Clo - c_Clo_old) * phi6 * dx 

FF = F_K + F_Na + F_Cl	


#Modifications for using specified solver
bc=[]
# Compute Jacobian form
J = derivative(FF, w)

problem = NonlinearVariationalProblem(FF, w, bc, J)
solver = NonlinearVariationalSolver(problem)
parameters["krylov_solver"]["monitor_convergence"] = True
parameters["krylov_solver"]["report"] = True		
parameters["krylov_solver"]["nonzero_initial_guess"] = True	

solver.parameters["newton_solver"]["relaxation_parameter"] = 1.0
solver.parameters["newton_solver"]["relative_tolerance"] = 1.0e-4	
solver.parameters["newton_solver"]["absolute_tolerance"] = 1.0e-8
solver.parameters["newton_solver"]["maximum_iterations"] = 100



plot(c_Ki + c_Ko, key='c_K')
plot(c_Ko, key='c_Ko')
#plot(j_KM, key='j_KM')




#import IPython
#IPython.embed()


while t < simt + DOLFIN_EPS:
	solver.solve()
	t += dt
	w_old.assign(w)
	
	#Plotting

	time = str(t)
	plot(c_Ki + c_Ko, key='c_K', title = time)	
	plot(c_Ko, key='c_Ko', title = time)	
#	plot(j_KM, key='j_KM', title = time)




interactive()

