from dolfin import *

mesh = Mesh("single_cell.xml")

xmin, xmax = -10.0, 90.0
ymin, ymax = -10.0, 90.0
zmin, zmax = 0.0, 30.0

# plot(mesh)
# interactive()

boundary_parts = FacetFunction("size_t", mesh) # 

class Noslip(SubDomain):        # add noslip BC on everything
    def inside(self, x, on_boundary):
        return on_boundary

noslip = Noslip()
noslip.mark(boundary_parts, 3)

    
class Inflow(SubDomain):        # remark inflow and outflow, on which we don't want noslip BCs
    def inside(self, x, on_boundary):
                return near(x[0], xmin) and on_boundary             

inflow = Inflow()
inflow.mark(boundary_parts, 1)

class Outflow(SubDomain):
    def inside(self, x, on_boundary):
                return near(x[0], xmax) and on_boundary             

outflow = Outflow()
outflow.mark(boundary_parts, 2)


plot(boundary_parts)

interactive()



