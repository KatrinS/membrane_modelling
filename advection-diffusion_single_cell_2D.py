from dolfin import *

# Load mesh 
mesh = Mesh("./single_cell_2D.xml")

##########
xmin, xmax = -10.0, 90.0
ymin, ymax = -10.0, 90.0

# Sub domain for inflow (left)
class Inflow(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[0], xmin) and on_boundary 

boundaries = FacetFunction('size_t', mesh)
boundaries.set_all(3)

# Mark inflow as sub domain 1
inflow = Inflow()
inflow.mark(boundaries, 1)

plot(boundaries)
##########

# Create FunctionSpaces
Q = FunctionSpace(mesh, "CG", 1)
V = VectorFunctionSpace(mesh, "CG", 2)

# Create velocity Function from file
velocity = Function(V);
File("velocity.xml") >> velocity
#velocity = Constant((0.1, 0))

# Initialise source function and previous solution function
f  = Constant(0.0)
u0 = Function(Q)

# Parameters
T = 1.0
delta_t = 0.01
D = 0.00005 #TODO change to realistic value

# Test and trial functions
u, v = TrialFunction(Q), TestFunction(Q)

# Mid-point solution
u_mid = 0.5*(u0 + u)

# Residual
r = u - u0 + delta_t*(dot(velocity, grad(u_mid)) - D*div(grad(u_mid)) - f)

# Galerkin variational problem
F = v*(u - u0)*dx + delta_t*(v*dot(velocity, grad(u_mid))*dx \
                      + D*dot(grad(v), grad(u_mid))*dx)

# Add SUPG stabilisation terms
vnorm = sqrt(dot(velocity, velocity))
h = CellSize(mesh)
F += (h/(2.0*vnorm))*dot(velocity, grad(v))*r*dx

# Create bilinear and linear forms
a = lhs(F)
L = rhs(F)

# Set up boundary condition
def boundary_value(n):   #TODO: change to realistic values
	return 1.0
#    if n < 10:
#        return float(n)/10.0
#    else:
#        return 1.0

g = Constant(boundary_value(0)) 
bc = DirichletBC(Q, g, boundaries, 1)

# Assemble matrix
A = assemble(a)
bc.apply(A)

# Create linear solver and factorize matrix
solver = LUSolver(A)
solver.parameters["reuse_factorization"] = True

# Output file
out_file = File("results/concentration.pvd")

# Set intial condition
u = u0

# Time-stepping
t = 0.0
while t < T:

    # Assemble vector and apply boundary conditions
    b = assemble(L)
    bc.apply(b)

    # Solve the linear system (re-use the already factorized matrix A)
    solver.solve(u.vector(), b)
    #solve(a == L, u, bc)

    # Copy solution from previous interval
    u0 = u

    # Plot solution
    plot(u)

    # Save the solution to file
    out_file << (u, t)

    # Move to next interval and adjust boundary condition
    t += delta_t
    g.assign(boundary_value(int(t/delta_t)))

# Hold plot
interactive()
